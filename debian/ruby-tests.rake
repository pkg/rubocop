require 'gem2deb/rake/spectask'

ENV['LANG'] = 'C.UTF-8'

skip = [
  'spec/rubocop/config_loader_spec.rb',
  'spec/project_spec.rb',
  'spec/rubocop/cli/options_spec.rb',
  'spec/rubocop/cli/suggest_extensions_spec.rb',
  'spec/rubocop/config_obsoletion_spec.rb',
  'spec/rubocop/lockfile_spec.rb',
  'spec/rubocop/server/rubocop_server_spec.rb',
  'spec/rubocop/version_spec.rb',
]
if ENV['AUTOPKGTEST_TMP']
  skip << './spec/rubocop/cop/generator_spec.rb'
end

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.exclude_pattern = skip.join(',')
end
